package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void setFlyBehavior(FlyBehavior fly){
        this.flyBehavior = fly;
    }

    
    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public void setQuackBehavior(QuackBehavior quack){
        this.quackBehavior = quack;
    }

    public abstract void display() ;

    public void swim() {
        System.out.println("All ducks swim");
    }

}
