package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior{
    
    // TODO Complete me !
    @Override 
    // override dari class FlyBehavior
    public void fly() {
        System.out.println("I can't fly :( ");
    }
}
