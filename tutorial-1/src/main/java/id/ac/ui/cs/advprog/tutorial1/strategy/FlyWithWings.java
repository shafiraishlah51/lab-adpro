package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior{
   
    @Override 
    // override dari class FlyBehavior
    public void fly() {
        System.out.println("I'm flying my wonderful wings !");
    }
}
