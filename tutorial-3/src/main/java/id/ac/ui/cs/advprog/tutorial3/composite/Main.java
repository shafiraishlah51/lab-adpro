package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main {

    public static void main(String args[]){
        Company abc = new Company();
        abc.addEmployee(new FrontendProgrammer("Messi",30001));
        abc.addEmployee(new BackendProgrammer("Cristiano",20001));
        abc.addEmployee(new NetworkExpert("Kylian",50001));
        abc.addEmployee(new SecurityExpert("Ramos",70001));
        abc.addEmployee(new UiUxDesigner("Pogboom",90001));
        abc.addEmployee(new Ceo("Mourinho",200001));
        abc.addEmployee(new Cto("Guardiola",100001));
        System.out.println(abc.getNetSalaries());
        for(Employees e: abc.getAllEmployees()){
            System.out.println(e.name+" kerjaannya sebagai "+e.role+" dibayar dengan gaji "+e.salary);
        }
    }
}
