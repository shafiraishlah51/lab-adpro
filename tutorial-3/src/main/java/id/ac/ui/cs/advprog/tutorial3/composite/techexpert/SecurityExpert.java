package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        //TODO Implement
        if (salary < 70000.00){
            throw new IllegalArgumentException();
        }
        super.name = name;
        super.salary = salary;
        role = "Security Expert";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    public String getRole() {
        return role;
    }
}
