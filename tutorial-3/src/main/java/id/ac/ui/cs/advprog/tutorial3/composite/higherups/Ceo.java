package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        //TODO Implement
        if (salary < 200000.00){
            throw new IllegalArgumentException();
        }
        super.name = name;
        super.salary = salary;
        role = "CEO" ;

    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

}
