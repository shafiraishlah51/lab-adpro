package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        //TODO Implement
        if (salary < 30000.00) {
            throw new IllegalArgumentException();
        }
        super.name = name;
        super.salary = salary;
        role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }

    public String getRole() {
        return role;
    }
}
